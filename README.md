**Projet Middleware: Site de partage de photos (2019/2020)** 

**Deployé et accessible via** : [https://morning-headland-23954.herokuapp.com/](https://morning-headland-23954.herokuapp.com/) <br />
**Documentation de l'API** (Swagger) disponible en local via [http://localhost:8081/api-docs/](http://localhost:8081/api-docs/) <br />
ou bien en ligne via : [https://morning-headland-23954.herokuapp.com/api-docs/](https://morning-headland-23954.herokuapp.com/api-docs/)  

<hr />



(Windows) : <br />
**Pré-requis :** <br />
Pour tester le projet, cloner `git clone <url projet https>` <br />
Ou télécharger le zip puis installez les outils suivants: <br />
<br />
**NODE.JS** <br />
Vous devez télécharger et installer la dernière version LTS (la version 12.14.0 de Node.js) : 
[Télécharger NodeJs](https://nodejs.org/en/download/) <br />
<br />
**NPM (le package manager)** <br />
Pour l’installer, ouvrez une ligne de commande et tapez la commande suivante :  `npm install -g npm@latest` <br />  
Si cette commande échoue pour un problème de permission sur Mac ou Linux, vous pouvez la lancer en mode super-utilisateur  avec sudo. <br />
<br />
**REACT/CRA(Create-React-App)** <br />
Installer React JS:
`npm install --global create-react-app` <br />
<br />
**MONGO DB :** <br />
Vous devez télécharger et installer la base de données MongoDB :
[Télécharger mongodb](https://www.mongodb.com/download-center/community) <br />
<br />
**Contenu :** <br />
BackEnd "showme_api" 
FrontEnd "showme_gui" <br />
<br />
**Liste de commande :** <br />
Avant de lancer le projet, installer les dépendances. <br />
Entez dans le dossier "/showme_api" puis tapez: `npm i` (s'il présente des vulnérabilités tapez: `npm ci`). <br />
Puis entrez dans le dossier "/showme_gui" puis tapez: `npm i` <br />
Pour démarrer notre projet, lancez "npm start" dans "/showme_api" pour lancer le serveur et "npm start" dans "/showme_gui" pour l'interface.  <br />
Dans le navigateur de votre choix  tapez: [http://localhost:3000](http://localhost:3000) ouvrir l'application. <br />
En ce qui concernent la deuxième méthode pour lancer l'application, c'est de se rendre directement sur le lien suivant. : https://morning-headland-23954.herokuapp.com <br />
(Version déployée en ligne).
<br />
<br />
**TESTS UNITAIRES :** <br />
Pour démarrer les tests unitaires faut se rendre sur le repertoire showme_api\test et de taper la commande suivante : `npm test` <br />
Les 2 fichiers de tests pour utilisateur et invitation se lancent.<br />
<b>Note : </b> il y a des tests ou il faut introduire le token utilisateur pour les exécuter (question de sécurité), du coup pour les tests qui demandent ça <br />
il suffit de générer un token, en se connectant avec @mail et le mot de passe d'un utilisateur existant (donc le créer) avec la route (POST /users/login)<br />
sur Postman et de récupérer le Token depuis la réponse (la durée de validité du Token est d'une heure pour des questions de sécurité).
