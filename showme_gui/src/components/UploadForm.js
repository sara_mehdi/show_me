import React, { Component } from "react";

import axios from "axios";
import { Button, TextField, MenuItem } from "@material-ui/core";
import withStyles from "@material-ui/core/styles/withStyles";
import MySnackBar from "../util/MySnackBar";

const styles = theme => ({
  ...theme.spreadIt
});

class UploadForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      legend: "",
      visibility: "public",
      picture: null,
      preview: null,
      errors: []
    };
  }

  validateUpload = (picture, legend) => {
    const errors = [];
    if (picture === null) {
      errors.push("Image is required!");
    }
    if (legend.trim() === "") {
      errors.push("Legend must not be empty!");
    }
    if (legend.length > 40) {
      errors.push("Legend is too long! (Max: 40 characters)");
    }
    if (legend.trim() !== "" && legend.length < 2) {
      errors.push("Legend is too short! (Min: 2 characters)");
    }
    return errors;
  };

  handleSubmit = event => {
    event.preventDefault();
    const { picture, legend } = this.state;
    const errors = this.validateUpload(picture, legend);

    if (errors.length > 0) {
      this.setState({ errors: errors });
    } else {
      const formData = new FormData();
      formData.append("legend", this.state.legend);
      formData.append("visibility", this.state.visibility);
      formData.append("picture", this.state.picture);

      axios
        .post(`/pictures/`, formData)
        .then(response => {
          this.props.toSuccess(); //affiche le success
          //this.props.toClose(); // ferme le dialog
          window.location.reload();
        })
        .catch(err => console.log(err));
    }
  };

  closeErrorSnackbar = event => {
    const err = this.state.errors.slice();
      const index = err.indexOf(event.target.key);
      err.splice(index, 1);
      this.setState({ errors: err });
  };

  handleChange = event => {
    this.setState({
      legend: event.target.value
    });
  };

  handleChangeSelect = event => {
    this.setState({
      visibility: event.target.value
    });
  };

  handleChangeFile = event => {
    this.setState({
      picture: event.target.files[0],
      preview: URL.createObjectURL(event.target.files[0])
    });
  };

  render() {
    const { errors } = this.state;
    const pic = this.state.preview;
    const { classes } = this.props;

    let imagePreview;
    if (pic) {
      imagePreview = (
        <div>
          <img src={this.state.preview} alt=" " width={240} height={143} />
        </div>
      );
    }

    return (
      <form onSubmit={this.handleSubmit}>
        <div>
          {errors.map(error => (
            <MySnackBar
              key={error}
              variant="error"
              message={error}
              onClose={this.closeErrorSnackbar}
              className={classes.myMargin}
            />
          ))}
        </div>
        {imagePreview}
        <div>
          <input type="file" name="file" onChange={this.handleChangeFile} />
        </div>
        <div>
          <TextField
            className={classes.field}
            id="legend"
            label="Legend"
            multiline
            rowsMax="2"
            onChange={this.handleChange}
            margin="normal"
            variant="outlined"
          />
        </div>
        <div>
          <TextField
            className={classes.field}
            id="visibility"
            select
            label="Visibility"
            value={this.state.visibility}
            onChange={this.handleChangeSelect}
            margin="normal"
            variant="outlined"
          >
            <MenuItem key="1" value="public">
              {"Public"}
            </MenuItem>
            <MenuItem key="2" value="prive">
              {"Private"}{" "}
            </MenuItem>
            <MenuItem key="3" value="amis">
              {"Friend"}
            </MenuItem>
          </TextField>
        </div>

        <Button
          className={classes.myMargin}
          type="submit"
          variant="contained"
          color="primary"
          fullWidth
        >
          {"Add picture"}
        </Button>
      </form>
    );
  }
}

export default withStyles(styles)(UploadForm);
