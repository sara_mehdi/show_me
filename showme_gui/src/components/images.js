import React, { Component } from "react";

import axios from "axios";
import Gallery from "react-grid-gallery";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = theme => ({
  ...theme.spreadIt
});

const customTagStyle = {
  wordWrap: "break-word",
  display: "inline-block",
  backgroundColor: "white",
  height: "auto",
  fontSize: "75%",
  fontWeight: "600",
  lineHeight: "1",
  padding: ".2em .6em .3em",
  borderRadius: ".25em",
  color: "black",
  verticalAlign: "baseline",
  margin: "2px"
};

export class Images extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pictures: [],
      currentIndex: 0
    };
  }

  componentDidMount() {
    this.getAllUserPictures();
  }

  onCurrentImageChange = index => {
    this.setState({ currentIndex: index });
  };

  setCustomTags = i => {
    return i.tags.map(t => {
      return (
        <div key={t.value} style={customTagStyle}>
          {t.title}
        </div>
      );
    });
  };

  //retrieve all user picture in visibility public
  getAllUserPictures = () => {
    const decode = "data:image/jpeg;base64,";

    axios
      .get(`/pictures`)
      .then(response => {
        let array = this.state.pictures.slice();
        response.data.pictures.forEach(element => {
          const picture = {
            _id: element._id,
            time: element.time,
            caption: element.legend,
            visibility: element.visibility,
            src: decode + element.base64,
            thumbnail: decode + element.base64,
            thumbnailWidth: 320,
            thumbnailHeight: 212,
            user: element.user,
            tags: [{ value: element.legend, title: element.legend }]
          };
          array.push(picture);
        });
        this.setState({
          pictures: array
        });
      })
      .catch(err => console.log(err));
  };

  render() {
    const { classes } = this.props;
    var images = this.state.pictures.map(i => {
      i.customOverlay = (
        <div className={classes.captionStyle}>
          <div>
            By @{i.user.firstName} {i.user.lastName}
          </div>
          {Object.prototype.hasOwnProperty.call(i, "tags") &&
            this.setCustomTags(i)}
        </div>
      );
      return i;
    });

    return (
      <div>
        <Gallery
          images={images}
          enableLightbox={true}
          enableImageSelection={false}
          currentImageWillChange={this.onCurrentImageChange}
        />
      </div>
    );
  }
}

export default withStyles(styles)(Images);
