import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { Link } from 'react-router-dom';
import EditDetails from '../components/EditDetails';
import MyButton from '../util/myButton';
//MUI Stuff
import Button from '@material-ui/core/Button';
import { Paper } from '@material-ui/core';
import MuiLink from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';


//Redux stuff
import { connect } from 'react-redux';
import { logoutUser, uploadImage } from "../redux/actions/userActions";

import axios from 'axios';
import photoDefault from '../images/profile.png';
// Icons
import LinkIcon from '@material-ui/icons/Link';
import EditIcon from '@material-ui/icons/Edit';

import AccessibilityNewIcon from '@material-ui/icons/AccessibilityNew';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
const styles = (theme) => ({
    ...theme.spreadIt,
    button: {
        float: 'right'
    }
});



class Profile extends Component {

    handleImageChange = (event) => {
        const image = event.target.files[0];
        //send to server
        const formData = new FormData();
        //formData.append("picture", this.state.picture);
        formData.append('picture', image)
        this.props.uploadImage(formData);
    };

    handleEditPicture = () => {
        const fileInput = document.getElementById('imageInput');
        fileInput.click();
    }
    handleDelete = () => {

        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure to do this.',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        axios.delete(`/users/`)
                            .then(res => {
                                this.props.logoutUser()
                            })
                            .catch(err => {
                                alert({err})
                            });
                    }
                },
                {
                    label: 'No',
                    onClick: () => {}
                }
            ]
        })
        //this.props.logoutUser();
        /**/
    }
    render() {
        const { classes, user: { credentials: { _id, firstName, lastName, email, profilePhoto },
            loading, authenticated } } = this.props;
        const decode = "data:image/jpeg;base64,";
        const photo = decode + profilePhoto;
        let profileMarkup = !loading ? (authenticated ? (
            <Paper className={classes.paper}>
                <div className={classes.profile}>
                    <div className="image-wrapper">
                        <img src={profilePhoto ? photo : photoDefault} alt="profile" className="profile-image" />
                        <input type="file" name="file" id="imageInput" hidden="hidden" onChange={this.handleImageChange} />
                        <MyButton
                            tip="Edit profile picture"
                            onClick={this.handleEditPicture}
                            btnClassName="button"
                        >
                            <EditIcon color="primary" />
                        </MyButton>
                    </div>
                    <hr />
                    <div className="profile-details">
                        <MuiLink component={Link} to={`/myphotos/${_id}`} color="primary" variant="h5">
                            {`${firstName} ${lastName}`}
                        </MuiLink>
                        <hr />

                        {email && (
                            <Fragment>
                                <LinkIcon color="primary" />
                                <a href={email} target="_bank" rel="noopener noreferrer">
                                    {' '}{email}
                                </a>
                            </Fragment>
                        )}
                        <hr />
                        <AccessibilityNewIcon color="primary" />
                        <span>Welcome To showMe</span>
                    </div>


                    <MyButton tip="delete your profile" onClick={this.handleDelete}>
                        <DeleteForeverIcon color="secondary" />
                    </MyButton>
                    <EditDetails />
                </div>
            </Paper>
        ) : (
                <Paper className="classes.paper">
                    <Typography variant="body2" align="center">
                        No profile found, please login again
                </Typography>
                    <div className={classes.buttons}>
                        <Button variant="contained" color="primary" component={Link} to="/login">Login</Button>
                        <Button variant="contained" color="secondary" component={Link} to="/signup">Sign Up</Button>
                    </div>
                </Paper>
            )) : (<p> loading </p>)
        return profileMarkup;
    }
}

Profile.propTypes = {
    classes: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    logoutUser: PropTypes.func.isRequired,
    uploadImage: PropTypes.func.isRequired

}


const mapStateToProps = (state) => ({
    user: state.user,

});

const mapActionsToProps = {
    logoutUser,
    uploadImage
}

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles)(Profile));
