import React, { Component, Fragment } from 'react';

import Button from '@material-ui/core/Button';
// MUI stuff
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
// Icons

import PeopleAltIcon from '@material-ui/icons/PeopleAlt';

import PersonIcon from '@material-ui/icons/Person';

import axios from "axios";

const styles = {
    button: {
        marginTop: 20,
        position: 'relative'
    }
}

class Invitations extends Component {
    state = {
        anchorEl: null,
        invitations: []
    }

    componentDidMount(){
        axios
            .get(`/invitations/inviUser`)
            .then(res => {
                this.setState({
                    invitations: res.data
                });
            })
            .catch(err => console.log(err));
    }
    handleSubmit = (id) => {
        console.log(id);

        const invitation = { senderId: id }
        axios.put('/users/accept', invitation)
            .then(res => {
                window.location.reload()
            })
            .catch(err => {

            });

    }

    handleOpen = (event) => {
        this.setState({ anchorEl: event.target });
    };
    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    render() {
        const invitations = this.state.invitations;
        const anchorEl = this.state.anchorEl;

        let invitationsIcon;
        if (invitations && invitations.length > 0) {
                invitationsIcon = (
                    <Badge
                        badgeContent={
                            invitations.length
                        }
                        color="secondary"
                    >
                        <PeopleAltIcon />
                    </Badge>
                )
                 
        } else {
            invitationsIcon = <PeopleAltIcon />;
        }
        let invitationsMarkup =
            invitations && invitations.length > 0 ? (
                invitations.map((not) => {
                    const firstName = not.firstName;
                    const lastName = not.lastName;
                    const status = not.status
                    const senderId= not._id
                    return (
                        <MenuItem key={not.createdAt} onClick={this.handleClose}>
                            <PersonIcon />
                            <Typography

                            >
                                {firstName} {lastName}
                                {!status && <Button type="submit" variant="contained" color="primary" className={styles.button} disabled={this.state.status} onClick={() => {this.handleSubmit(senderId) }}
                                >Accept</Button>}

                            </Typography>

                        </MenuItem>

                    );
                })
            ) : (
                    <MenuItem onClick={this.handleClose}>
                        You have no invitations yet
          </MenuItem>
                );
        return (
            <Fragment>
                <Tooltip placement="top" title="Invitations">
                    <IconButton
                        aria-owns={anchorEl ? 'simple-menu' : undefined}
                        aria-haspopup="true"
                        onClick={this.handleOpen}
                    >
                        {invitationsIcon}
                    </IconButton>
                </Tooltip>
                <Menu
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleClose}
                    onEntered={this.onMenuOpened}
                >
                    {invitationsMarkup}
                </Menu>
            </Fragment>

        )
    }
}


export default Invitations
