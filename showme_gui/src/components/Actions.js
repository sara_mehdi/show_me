import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { Link } from 'react-router-dom';
import dayjs from 'dayjs';
import EditDetails from '../components/EditDetails';
//MUI Stuff
import Button from '@material-ui/core/Button';
import { Paper, Toolbar } from '@material-ui/core';
import MuiLink from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

//Redux stuff
import { connect } from 'react-redux';
import { logoutUser, uploadImage } from "../redux/actions/userActions";

import AppIcon from '../images/alien.png';
// Icons
import LocationOn from '@material-ui/icons/LocationOn';
import LinkIcon from '@material-ui/icons/Link';
import CalendarToday from '@material-ui/icons/CalendarToday';
import EditIcon from '@material-ui/icons/Edit';
import KeyboardReturn from '@material-ui/icons/KeyboardReturn';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const styles = (theme) => ({
    ...theme.spreadIt
});

class Actions extends Component {
    handleImageChange = (event) => {
        const image = event.target.files[0];
        //send to server
        const formData = new FormData();
        formData.append('image', image, image.name)
        this.props.uploadImage(formData);
    };
    handleEditPicture = () => {
        const fileInput = document.getElementById('imageInput');
        fileInput.click();
    }
    handleLogout = () => {
        this.props.logoutUser();
    }
    render() {
        const { classes, user: { credentials: { _id, firstName, imageUrl, lastName, email },
            loading, authenticated } } = this.props;
        let ActionsMarkup = !loading ? (authenticated ? (
            <Paper className={classes.paper}>
                <div className={classes.Actions}>
                    
                </div>
            </Paper>
        ) : (
                <Paper className="classes.paper">
                    <Typography variant="body2" align="center">
                        No Actions found, please login again
                </Typography>
                    <div className={classes.buttons}>
                        <Button variant="contained" color="primary" component={Link} to="/login">Login</Button>
                        <Button variant="contained" color="secondary" component={Link} to="/signup">Sign Up</Button>
                    </div>
                </Paper>
            )) : (<p> loading </p>)
        return ActionsMarkup;
    }
}

Actions.propTypes = {
    classes: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    logoutUser: PropTypes.func.isRequired,
    uploadImage: PropTypes.func.isRequired

}


const mapStateToProps = (state) => ({
    user: state.user,
    
});

const mapActionsToProps = {
    logoutUser,
    uploadImage
}

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles)(Actions));
