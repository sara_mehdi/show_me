import React, { Component,Fragment } from 'react'
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { Link } from 'react-router-dom';

import photoDefault from '../images/profile.png';
//MUI Stuff
import Button from '@material-ui/core/Button';
import { Paper } from '@material-ui/core';
import MuiLink from '@material-ui/core/Link';


//Redux stuff
import { connect } from 'react-redux';



// Icons

import LinkIcon from '@material-ui/icons/Link';

import axios from 'axios';

const styles = (theme) => ({
    ...theme.spreadIt
});

class ProfileFriend extends Component {

    constructor(props) {
        super(props);

        this.state = {
            buttonDisable: false,
            buttonText: "Send Invitation",
            myInvitations: [],
            sentInvitations: []
        }


    }

    componentDidMount() {
        axios
            .get(`/invitations/inviUser`)
            .then(res => {

                this.setState({
                    myInvitations: res.data
                });
            })
            .catch(err => console.log(err));

        axios
            .get(`/invitations/send`)
            .then(res => {
                this.setState({
                    sentInvitations: res.data.invitation
                });
            })
            .catch(err => console.log(err));
    }



    handleSubmit = (id) => {

        this.setState({
            buttonDisable: true,
            buttonText: "Invitation is sent"
        })
        const invitations = { idReceiver: id }
        axios.post('/invitations/', invitations)
            .then(res => {

            })
            .catch(err => {

            });

    }

    handleAccept = (id) => {

        const invitation = { senderId: id }
        axios.put('/users/accept', invitation)
            .then(res => {
                window.location.reload()
            })
            .catch(err => {

            });
    }
    handleDelete = (id)=>{
        axios.delete(`/users/deletefriend/${id}`)
        .then(res => {
            window.location.reload()
        })
        .catch(err => {

        });
    }

    handleLogout = () => {
        this.props.logoutUser();
    }
    render() {
        const { classes, friend: { _id, firstName, lastName, email, profilePhoto }, user: { credentials: { friends },
             } } = this.props;
        const decode = "data:image/jpeg;base64,";
        const photo = decode + profilePhoto;
        let isFriend = -2;
        let isInvited = -2
        if (friends) {
            const list = friends.map(e => e._id.toString());
            isFriend = list.indexOf(_id);
        }
        if (this.state.myInvitations.length > 0) {
            const list2 = this.state.myInvitations.map(e => e._id.toString());
            isInvited = list2.indexOf(_id);
        }
        let test = false;
        if (this.state.sentInvitations.length > 0) {
            const list3 = this.state.sentInvitations.map(e => e.idReceiver.toString());
            if (list3.indexOf(_id) > -1) {
                test = true;
            };
        }

        let profileMarkup =
            <Paper className={classes.paper}>
                <div className={classes.profile}>
                    <div className="image-wrapper">
                        <img src={profilePhoto? photo:photoDefault} alt="profile" className="profile-image" />
                    </div>
                    <hr />
                    <div className="profile-details">
                        <MuiLink component={Link} to={`/user/${_id}`} color="primary" variant="h5">
                            {`${firstName} ${lastName}`}
                        </MuiLink>
                        <hr />

                        {email && (
                            <Fragment>
                                <LinkIcon color="primary" />
                                <a href={email} target="_bank" rel="noopener noreferrer">
                                    {email}
                                </a>
                            </Fragment>
                        )}
                        <hr />

                    </div>
                    {(isFriend === -1 && (isInvited === -2 || isInvited === -1)) && <Button type="submit" variant="contained" color="primary" disabled={this.state.buttonDisable || test} className={classes.button} onClick={() => this.handleSubmit(_id)}
                    >{this.state.buttonText}</Button>}
                    {(isFriend === -1 && isInvited !== -2 && isInvited !== -1) && <Button type="submit" variant="contained" color="primary" disabled={this.state.buttonDisable} className={classes.button} onClick={() => this.handleAccept(_id)}
                    >Accept invitation </Button>}
                    {(isFriend > -1 ) && <Button type="submit" variant="contained" color="secondary"  className={classes.button} onClick={() => this.handleDelete(_id)}
                    >delete</Button>}
                </div>
            </Paper >

        return profileMarkup;
    }
}

ProfileFriend.propTypes = {
    classes: PropTypes.object.isRequired,
    friend: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    user: state.user,

});



export default connect(mapStateToProps)(withStyles(styles)(ProfileFriend));
