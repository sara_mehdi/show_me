import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';

// MUI Stuff
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import MyButton from '../util/myButton';
//Redux stuff
import { connect } from 'react-redux';
import { editUserDetails } from "../redux/actions/userActions";

// Icons
import EditIcon from '@material-ui/icons/Edit';

const styles = (theme) => ({
    ...theme.spreadIt,
    button: {
        float: 'right'
    }
});


class EditDetails extends Component {
    state = {
        firstName: '',
        lastName: '',
        open: false
    }

    mapUserDetailsToState = (credentials) => {
        this.setState({
            firstName: credentials.firstName ? credentials.firstName : '',
            lastName: credentials.lastName ? credentials.lastName : '',
        })
    }

    componentDidMount() {
        this.mapUserDetailsToState(this.props.credentials);
    }


    handleOpen = () => {
        this.setState({ open: true });
        this.mapUserDetailsToState(this.props.credentials);
    }
    handleClose = () => {
        this.setState({ open: false });
    }


    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    handleSubmit = () => {
        const userDetails = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
        };
        this.props.editUserDetails(userDetails);
        this.handleClose();

    }
    render() {
        const { classes } = this.props;
        return (
            <Fragment>
                <MyButton
                    tip="Edit Details"
                    onClick={this.handleOpen}
                    btnClassName={classes.button}
                >
                    <EditIcon color="primary" />
                </MyButton>
                <Dialog open={this.state.open} onClose={this.handleClose} fullWidth maxWidth="sm">
                    <DialogTitle> Edit your details</DialogTitle>
                    <DialogContent>
                        <form>
                            <TextField name="firstName" type="text" label="FirstName" multiline rows="3" placeholder="firstName"
                                className={classes.TextField} value={this.state.firstName} onChange={this.handleChange} fullWidth />
                            <TextField name="lastName" type="text" label="LastName" placeholder="lastName"
                                className={classes.TextField} value={this.state.lastName} onChange={this.handleChange} fullWidth />
                        </form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                    </Button>
                        <Button onClick={this.handleSubmit} color="primary">
                            Save
                    </Button>
                    </DialogActions>
                </Dialog>
            </Fragment>
        )
    }
}

EditDetails.propTypes = {
    classes: PropTypes.object.isRequired,
    editUserDetails: PropTypes.func.isRequired,

}
const mapStateToProps = (state) => ({
    credentials: state.user.credentials,
});
export default connect(mapStateToProps, { editUserDetails })(withStyles(styles)(EditDetails));
