import React, { Component, Fragment } from "react";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import PropTypes from "prop-types";
import MyButton from "../util/myButton";
import { Link } from "react-router-dom";
import withStyles from "@material-ui/core/styles/withStyles";

import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
//Redux stuff
import { connect } from "react-redux";

// Icons
import AddIcon from "@material-ui/icons/Add";
import HomeIcon from "@material-ui/icons/Home";
import Invitatiion from "./Invitations";
// Pop up
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { logoutUser } from "../redux/actions/userActions";
import UploadForm from "./UploadForm";
import Snackbar from "@material-ui/core/Snackbar";
import MySnackBar from "../util/MySnackBar";
import { Typography } from "@material-ui/core";

const styles = theme => ({
  ...theme.spreadIt
});
class Navbar extends Component {
  constructor() {
    super();
    this.state = {
      open: false,
      afterSubmited: false
    };
  }

  toggleSuccess = () => {
    this.setState({
      afterSubmited: true
    });
  };

  closeSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({
      afterSubmited: false
    });
  };

  toggleModal = () => {
    this.setState({
      open: !this.state.open
    });
  };

handleLogout = () => {
  this.props.logoutUser();
}  
render() {
    const { authenticated, classes } = this.props;
    const { open, afterSubmited } = this.state;
    return (
      <AppBar>
        <Toolbar className="nav-container">
        <Typography variant="h5" noWrap>
            ShowMe
          </Typography>
          {authenticated ? (
            <Fragment>
              <Snackbar
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "center"
                }}
                open={afterSubmited}
                autoHideDuration={4000}
                onClose={this.closeSnackbar}
              >
                <MySnackBar
                  onClose={this.closeSnackbar}
                  variant="success"
                  message="The photo has been successfully added!"
                />
              </Snackbar>
              <MyButton tip="Post a picture" onClick={this.toggleModal}>
                <AddIcon color="primary" />
              </MyButton>
              <Dialog open={open} onClose={this.toggleModal}>
                <DialogTitle className={classes.headDialog}>
                  Add picture
                </DialogTitle>
                <DialogContent>
                  <UploadForm
                    toClose={this.toggleModal}
                    toSuccess={this.toggleSuccess}
                  />
                  <Button
                    variant="contained"
                    color="secondary"
                    fullWidth
                    onClick={this.toggleModal}
                  >
                    {"Cancel"}
                  </Button>
                </DialogContent>
              </Dialog>

              <Link to="/">
                <MyButton tip="Home">
                  <HomeIcon color="primary" />
                </MyButton>
              </Link>
              <Invitatiion />
              <Tooltip title="Logout" >
                <IconButton onClick={this.handleLogout}>
                  <ExitToAppIcon color="primary" />
                </IconButton>
              </Tooltip>
            </Fragment>

          ) : (
              <Fragment>
                <Button color="inherit" component={Link} to="/login"> Login </Button>
                <Button color="inherit" component={Link} to="/signup"> Signup </Button>
              </Fragment>

            )}


        </Toolbar>
      </AppBar>
    )
  }
}

Navbar.propTypes = {
  authenticated: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  authenticated: state.user.authenticated
});

const mapActionsToProps = {
  logoutUser
}

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles)(Navbar));
