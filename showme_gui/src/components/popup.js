import React from "react";


import {Link} from 'react-router-dom';
import "./style.scss";
import InfoIcon from "@material-ui/icons/Info";

import { List, ListItemText, ListItem, ListItemIcon } from "@material-ui/core";

class Popup extends React.Component {

  render() {
    const { items, isOpen } = this.props;
    //Do not show popup
    if (!isOpen) return null;
    return (
      <div className="popup">
        <div>
          <List>
            {items &&
              items.map((item, idx) => {
                return (
                  <ListItem button component={Link} to={`/friend/${item._id}`}>
                    <ListItemText
                      primary={item.first_name}
                      secondary={item.last_name}
                    />
                    <ListItemIcon> 
                      <InfoIcon />
                    </ListItemIcon>
                  </ListItem>
                );
              })}
            {items.length === 0 && (
              <div className="warning">
                <h3>Nothing found!</h3>
              </div>
            )}
          </List>
          <div className="footer">Type Keyword to search for an user</div>
        </div>
      </div>
    );
  }
}

export default Popup;
