import React from "react";
import Popup from "./popup";
import SearchIcon from "@material-ui/icons/Search";
import axios from 'axios';
import PropTypes from 'prop-types';

//Redux stuff
import { connect } from 'react-redux';
import { Grid, TextField } from "@material-ui/core";


function escapeRegExp(string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
}

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: [],
      isError: false,
      friends: [],
      foundFriends: [],
      showResult: false
    };
  }

  async fetchData() {
    axios.get(`/users/all`)
        .then(res => {

            this.setState({
                friends : res.data.users
            })
        })
        .catch(err => console.log(err))
  }

  componentDidMount() {
    this.fetchData();
  }

  setError(msg) {
    this.setState(prevState => ({ errors: [...prevState.errors, msg] }));
  }

  clearAllErrors() {
    this.setState({ errors: [] });
  }

  async searchFriend(keyword) {
    const { friends } = this.state;
    const { user } = this.props;
    keyword = escapeRegExp(keyword.toLowerCase());
    const pattern = `[A-Za-z.]*${keyword}[A-Za-z.]*`;
    const matchRegex = new RegExp(pattern);
    const foundFriends = friends.filter((item, idx) => {
      if (item._id !== user.credentials._id) {
        return (
          matchRegex.test(item.first_name.toLowerCase()) ||
          matchRegex.test(item.last_name.toLowerCase())
        );
      }
      return null;
    });
    this.setState({ foundFriends });
  }

  onInputChange(e) {
    const keyword = e.target.value;
    this.searchFriend(keyword);
  }

  onInput(e) {
    if (e.target.value !== "") this.showPopup();
    else this.hidePopup();
  }

  showPopup() {
    this.setState({ showResult: true });
  }

  hidePopup() {
    this.setState({ showResult: false });
  }

  render() {
    const { foundFriends, showResult } = this.state;
    return (
      <div>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <SearchIcon />
          </Grid>
          <Grid item>
            <TextField
              id="search"
              size="small"
              label="Search"
              variant="outlined"
              fullWidth
              onChange={this.onInputChange.bind(this)}
              onInput={this.onInput.bind(this)}
            />
          </Grid>
        </Grid>
        <Popup isOpen={showResult} items={foundFriends} />
      </div>
    );
  }
}

Search.propTypes = {
  user: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps)(Search);
