import React, { Component } from "react";

import axios from "axios";
import { Button, TextField, MenuItem } from "@material-ui/core";
import withStyles from "@material-ui/core/styles/withStyles";
import MySnackBar from "../util/MySnackBar";

const styles = theme => ({
  ...theme.spreadIt
});

class EditPicture extends Component {
  constructor(props) {
    super(props);
    this.state = {
      legend: "",
      visibility: "",
      preview: null,
      errors: []
    };
  }

  validateUpload = legend => {
    const errors = [];

    if (legend.trim() === "") {
      errors.push("Legend must not be empty!");
    }
    if (legend.length > 40) {
      errors.push("Legend is too long! (Max: 40 characters)");
    }
    if (legend.trim() !== "" && legend.length < 2) {
      errors.push("Legend is too short! (Min: 2 characters)");
    }
    return errors;
  };

  closeErrorSnackbar = event => {
    const err = this.state.errors.slice();
    const index = err.indexOf(event.target.key);
    err.splice(index, 1);
    this.setState({ errors: err });
  };

  componentDidMount() {
    this.setState({
      legend: this.props.photo.caption,
      visibility: this.props.photo.visibility,
      preview: this.props.photo.thumbnail
    });
  }

  handleChangeLegend = event => {
    this.setState({
      legend: event.target.value
    });
  };

  handleChangeSelect = event => {
    this.setState({
      visibility: event.target.value
    });
  };

  handleModify = event => {
    event.preventDefault();
    const { legend } = this.state;
    const errors = this.validateUpload(legend);
    const pictureId = this.props.photo._id;

    if (errors.length > 0) {
      this.setState({ errors: errors });
    } else {
      const pictureDetails = {
        legend: this.state.legend,
        visibility: this.state.visibility
      };
      axios
        .put(`/pictures/${pictureId}`, pictureDetails)
        .then(response => {
          this.props.toSuccess();
          this.props.toClose();
          window.location.reload();
        })
        .catch(err => console.log(err));
    }
  };

  render() {
    const { errors } = this.state;
    const { classes } = this.props;
    return (
      <form onSubmit={this.handleModify}>
        <div>
          {errors.map(error => (
            <MySnackBar
              key={error}
              variant="error"
              message={error}
              onClose={this.closeErrorSnackbar}
              className={classes.myMargin}
            />
          ))}
        </div>
        <img src={this.state.preview} alt=" " width={240} height={143} />

        <div>
          <TextField
            className={classes.field}
            id="legend"
            label="Legend"
            multiline
            rowsMax="2"
            onChange={this.handleChangeLegend}
            margin="normal"
            variant="outlined"
            value={this.state.legend}
          />
        </div>
        <div>
          <TextField
            className={classes.field}
            id="visibility"
            select
            label="Visibility"
            value={this.state.visibility}
            onChange={this.handleChangeSelect}
            margin="normal"
            variant="outlined"
          >
            <MenuItem key="1" value="public">
              {"Public"}
            </MenuItem>
            <MenuItem key="2" value="prive">
              {"Private"}{" "}
            </MenuItem>
            <MenuItem key="3" value="amis">
              {"Friend"}
            </MenuItem>
          </TextField>
        </div>

        <Button
          className={classes.myMargin}
          type="submit"
          variant="contained"
          color="primary"
          fullWidth
        >
          {"Accept change"}
        </Button>
      </form>
    );
  }
}

export default withStyles(styles)(EditPicture);
