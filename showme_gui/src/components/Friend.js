import React, { Component } from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
// MUI Stuff
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';


import AppIcon from '../images/profile.png';

const styles = {
    card: {
        display: 'flex',
        marginBottom: 10,
        height: 75,
        width: 200,
        border: "none",

    },
    image: {
        width: 50,
        height: 55,
        marginTop: 12,
        marginLeft: 10,
        borderRadius: 40,
        overflow: "hidden",

    },
    content: {
        padding: 15,
        objectFit: 'cover'
    }

}

export class Friend extends Component {
    render() {
        const { classes, friend: { firstName, lastName, email, _id } } = this.props;
        return (
            <Card className={classes.card}>
                <CardMedia image={AppIcon} title="Ptofile image" className={classes.image} />
                <CardContent className={classes.content}>
                    <Typography variant="body2"
                        component={Link}
                        to={`/friend/${_id}`}
                        color="primary"
                    > {firstName + " " + lastName} </Typography>
                    <br />
                    <Typography variant="body4">{email} </Typography>

                </CardContent>

            </Card>

        )
    }
}

export default withStyles(styles)(Friend);