import React, { Component } from "react";
import axios from "axios";
import Gallery from "react-grid-gallery";
import { Button, Paper, Grid } from "@material-ui/core";
import withStyles from "@material-ui/core/styles/withStyles";
import Snackbar from "@material-ui/core/Snackbar";
import MySnackBar from "../util/MySnackBar";
import EditPicture from "./EditPicture";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";

import { confirmAlert } from "react-confirm-alert"; // Import
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css

const styles = theme => ({
  ...theme.spreadIt
});

class MyPhotos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pictures: [],
      currentIndex: 0,
      openSnackbar: false,
      isDeleted: false,
      selection: false,
      photo: {},
      opened: false,
      afterModified: false,
      numberPicture: 0
    };
  }

  openSnackSuccess = () => {
    this.setState({
      afterModified: true
    });
  };

  toggleDialog = () => {
    this.setState({
      opened: !this.state.opened
    });
  };

  /**
   * Ferme le snackbar.
   */
  closeSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({
      openSnackbar: false,
      isDeleted: false,
      afterModified: false
    });
  };

  componentDidMount() {
    this.getAllPictures();
  }

  toggleSelection = () => {
    this.setState({
      selection: !this.state.selection
    });
  };

  onCurrentImageChange = index => {
    this.setState({ currentIndex: index });
  };

  onSelectImage = (index, image) => {
    var images = this.state.pictures.slice();
    var img = images[index];
    this.toggleDialog();
    this.setState({
      pictures: images,
      photo: img
    });
  };

  deleteOnePicture = pictureId => {
    axios
      .delete(`/pictures/one/${pictureId}`)
      .then(response => {
        window.location.reload();
      })
      .catch(error => {
        console.log("Erreur " + error);
      });
  };

  deleteImage = () => {
    var images = this.state.pictures.slice();
    var temp = images[this.state.currentIndex];
    images.splice(this.state.currentIndex, 1);
    this.deleteOnePicture(temp._id);
    this.setState({
      pictures: images,
      isDeleted: true
    });
  };

  getAllPictures = () => {
    const id = this.props.match.params._id;
    const decode = "data:image/jpeg;base64,";

    axios
      .get(`/pictures/all/${id}`)
      .then(response => {
        let array = this.state.pictures.slice();
        response.data.pictures.forEach(element => {
          const picture = {
            _id: element._id,
            time: element.time,
            caption: element.legend,
            visibility: element.visibility,
            src: decode + element.base64,
            thumbnail: decode + element.base64,
            thumbnailWidth: 320,
            thumbnailHeight: 212
          };
          array.push(picture);
        });
        this.setState({
          pictures: array
        });
      })
      .catch(err => console.log(err));
  };

  removeAllPhotos = () => {
    confirmAlert({
      title: "Confirm to delete",
      message: "Are you sure to delete all pictures?",
      buttons: [
        {
          label: "Yes",
          onClick: () => {
            axios
              .delete(`/pictures/all`)
              .then(response => {
                this.setState({ pictures: [], openSnackbar: true });
              })
              .catch(error => {
                console.log("Erreur " + error);
              });
          }
        },
        {
          label: "No",
          onClick: () => {}
        }
      ]
    });
  };

  render() {
    const { classes } = this.props;
    var images = this.state.pictures.map(i => {
      i.customOverlay = (
        <div className={classes.captionStyle}>
          <div>{i.caption}</div>
        </div>
      );
      return i;
    });
    return (
      <div>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left"
          }}
          open={this.state.openSnackbar}
          autoHideDuration={3000}
          onClose={this.closeSnackbar}
        >
          <MySnackBar
            onClose={this.closeSnackbar}
            variant="info"
            message="All photos have been deleted!"
          />
        </Snackbar>

        <div className={classes.containerU}>
        <Grid item xs={3}>
            <Paper className={classes.paper3}>
              <Button
              fullWidth
                variant="contained"
                color="secondary"
                onClick={this.removeAllPhotos}
              >
                {"Delete all"}{" "}
              </Button>
            </Paper>
        </Grid>
        <Grid item xs={3}>
            <Paper className={classes.paper3}>
              <div hidden={this.state.selection}>
                <Button
                fullWidth
                  variant="contained"
                  color="primary"
                  onClick={this.toggleSelection}
                >
                  {"Click to Edit"}
                </Button>
              </div>
              <div hidden={!this.state.selection}>
                <Button
                fullWidth
                  variant="contained"
                  color="secondary"
                  onClick={this.toggleSelection}
                >
                  {"Cancel modification"}
                </Button>
                {"(Click on top-left of picture to edit)"}
              </div>
            </Paper>
          </Grid>
        </div>
        <br />
        <Gallery
          images={images}
          enableLightbox={!this.state.selection}
          onSelectImage={this.onSelectImage}
          enableImageSelection={this.state.selection}
          currentImageWillChange={this.onCurrentImageChange}
          customControls={[
            <Button
              key={this.onCurrentImageChange}
              variant="contained"
              color="secondary"
              onClick={this.deleteImage}
            >
              Delete
            </Button>
          ]}
        />
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
          }}
          open={this.state.isDeleted}
          autoHideDuration={3000}
          onClose={this.closeSnackbar}
        >
          <MySnackBar
            onClose={this.closeSnackbar}
            variant="info"
            message="The photo has been deleted!"
          />
        </Snackbar>
        <Dialog open={this.state.opened} onClose={this.toggleDialog}>
          <DialogTitle className={classes.headDialog}>Edit Picture</DialogTitle>
          <DialogContent>
            <EditPicture
              toClose={this.toggleDialog}
              photo={this.state.photo}
              toSuccess={this.openSnackSuccess}
            />
            <Button
              variant="contained"
              color="secondary"
              fullWidth
              onClick={this.toggleDialog}
            >
              {"Cancel"}
            </Button>
          </DialogContent>
        </Dialog>
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "center"
          }}
          open={this.state.afterModified}
          autoHideDuration={4000}
          onClose={this.closeSnackbar}
        >
          <MySnackBar
            onClose={this.closeSnackbar}
            variant="success"
            message="The photo has been successfully edited!"
          />
        </Snackbar>
      </div>
    );
  }
}

export default withStyles(styles)(MyPhotos);
