import './App.css';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import jwtDecode from 'jwt-decode';
import axios from 'axios';

//Pages
import React, { Component } from 'react';
import home from './pages/home';
import login from './pages/login';
import signup from './pages/signup';
import FriendPic from './pages/FriendPic';

import themeFile from './util/theme';

//Redux
import { Provider } from 'react-redux';
import store from './redux/store';
import { SET_AUTHENTICATED } from './redux/types';
import { logoutUser, getUserData } from './redux/actions/userActions'


//Components
import Navbar from './components/Navbar';
import AuthRoute from './util/AuthRoute';
import PrivateRoute from './util/PrivateRoute';

import MyPhotos from './components/MyPhotos';



axios.defaults.baseURL = 'http://localhost:8081/';

const theme = createMuiTheme(themeFile);
const token = localStorage.token;
if (token) {
  const decodeToken = jwtDecode(token);
  if (decodeToken.exp * 1000 < Date.now()) {
    store.dispatch(logoutUser());
    window.location.href = '/login';
  } else {
    store.dispatch({ type: SET_AUTHENTICATED });
    axios.defaults.headers.common['Authorization'] = token;
    store.dispatch(getUserData());
  }
}


class App extends Component {
  render() {
    return (
        <MuiThemeProvider theme={theme} >
          <Provider store={store}>
            <Router>
              <Navbar />
              <div className="container" >
                <Switch>
                  <PrivateRoute exact path="/" component={home}  />
                  <AuthRoute exact path="/login" component={login} />
                  <AuthRoute exact path="/signup" component={signup} />
                  <PrivateRoute exact path="/friend/:_id" component={FriendPic} />
                  <PrivateRoute exact path="/myphotos/:_id" component={MyPhotos} />
                </Switch>
              </div>
            </Router>
          </Provider>

        </MuiThemeProvider>

    )
  }
}

export default App
