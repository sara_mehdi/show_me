import {SET_USER, SET_ERRORS, CLEAR_ERRORS,LOADING_UI,SET_UNAUTHENTICATED,LOADING_USER} from '../types';
import axios from 'axios';

export const loginUser = (userData,history) => (dispatch) =>{
    dispatch({type: LOADING_UI});
    axios.post('/users/login', userData)
    .then(res => {
        setAuthorization(res.data.token);
        dispatch(getUserData());
        dispatch({type: CLEAR_ERRORS});
        history.push('/');
    })
    .catch(err => {
        dispatch({
            type: SET_ERRORS,
            payload: err.response.data
        })
    });
}


export const getUserData = () => (dispatch) => {
    dispatch({type:LOADING_USER});
    axios.get('/users/user')
         .then(res => {
             dispatch({
                 type: SET_USER,
                 payload: res.data
             })
         })
         .catch(err => console.log(err))
} 

export const signupUser = (newUserData,history) => (dispatch) =>{
    dispatch({type: LOADING_UI});
    axios.post('/users/', newUserData)
    .then(res => {
        setAuthorization(res.data.token);
        dispatch(getUserData());
        dispatch({type: CLEAR_ERRORS});
        history.push('/');
    })
    .catch(err => {
        dispatch({
            type: SET_ERRORS,
            payload: err.response.data
        })
    });
}

export const logoutUser = () => (dispatch) =>{
    localStorage.removeItem('token');
    delete axios.defaults.headers.common['Authorization'];
    dispatch({type: SET_UNAUTHENTICATED});

}

export const uploadImage = (formData) => (dispatch) => {
    dispatch({type: LOADING_USER});
    axios.post('/users/post_photo_profile/',formData)
        .then(() => {
            dispatch(getUserData()); 
        })
        .catch(err => console.log(err)); 
}

export const editUserDetails = (userDetails)=>(dispatch) =>{
    dispatch({type: LOADING_USER});
    axios.patch('/users/',userDetails)
      .then(()=>{
          dispatch(getUserData());
      })
      .catch(err => console.log(err));
}



const setAuthorization = (tokenGeted) => {
    const token = `Bearer ${tokenGeted}`;
    localStorage.setItem('token',token);
    axios.defaults.headers.common['Authorization'] = token;
}   

