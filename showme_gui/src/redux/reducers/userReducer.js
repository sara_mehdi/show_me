import { SET_USER, LOADING_USER, SET_AUTHENTICATED,SET_UNAUTHENTICATED } from '../types';

const initialState = {
    authenticated: false,
    loading:false,
    credentials: {},
    request: ''

};

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_AUTHENTICATED:
            return {
                ...state,
                authenticated: true
            };
        case SET_UNAUTHENTICATED:
            return initialState;
        case SET_USER:
            return{
                authenticated: true,
                loading:false,
                credentials:action.payload.user,
                ...action.payload
            };
        case LOADING_USER:
            return {
                ...state,
                loading:true
            }
            default :
            return state

    }
}