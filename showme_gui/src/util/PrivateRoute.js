import React from 'react'
import {Route, Redirect} from 'react-router-dom';
import PropTypes from 'prop-types';

//Redux stuff
import { connect } from 'react-redux';

const  PrivateRoute = ({component: Component, authenticated,...rest}) => (
<Route {...rest}
 render={(props)=>
     authenticated===false ? <Redirect to = '/login'/>: <Component {...props} /> } />
)

PrivateRoute.propTypes={
    user : PropTypes.object.isRequired,
}

const mapStateToProps = (state) => ({
    authenticated: state.user.authenticated,
    user: state.user,
});

export default connect(mapStateToProps)(PrivateRoute)