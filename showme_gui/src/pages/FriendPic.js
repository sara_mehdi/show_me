import React, { Component } from "react";

import ProfileFriend from "../components/ProfileFriend";
import axios from "axios";
import Gallery from "react-grid-gallery";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = theme => ({
  ...theme.spreadIt
});

export class FriendPic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      friend: [],
      pictures: [],
      currentIndex: 0,
      loading:true
    };
  }
  componentDidMount() {
    this.getAllPictures();
    const id = this.props.match.params._id;
    axios
      .get(`/users/one/${id}`)
      .then(res => {
        this.setState({
          friend: res.data.user,
          loading:false
        });
      })
      .catch(err => console.log(err));
  }

  onCurrentImageChange = index => {
    this.setState({ currentIndex: index });
  };

  getAllPictures = () => {
    const id = this.props.match.params._id;
    const decode = "data:image/jpeg;base64,";

    axios
      .get(`/pictures/all/${id}`)
      .then(response => {
        let array = this.state.pictures.slice();
        response.data.pictures.forEach(element => {
          const picture = {
            _id: element._id,
            time: element.time,
            caption: element.legend,
            visibility: element.visibility,
            src: decode + element.base64,
            thumbnail: decode + element.base64,
            thumbnailWidth: 320,
            thumbnailHeight: 212
          };
          array.push(picture);
        });
        this.setState({
          pictures: array
        });
      })
      .catch(err => console.log(err));
  };

  render() {
    const { classes } = this.props;
    var images = this.state.pictures.map(i => {
      i.customOverlay = (
        <div className={classes.captionStyle}>
          <div>{i.caption}</div>
        </div>
      );
      return i;
    });

    return (
      !this.state.loading? (
      <div>
        <ProfileFriend friend={this.state.friend} />
        <br />
       
          <Gallery
            images={images}
            enableLightbox={true}
            enableImageSelection={false}
            currentImageWillChange={this.onCurrentImageChange}
          />

      </div>)
      :(
        <p> loading ....</p>
      )
    );
  }
}

export default withStyles(styles)(FriendPic);
