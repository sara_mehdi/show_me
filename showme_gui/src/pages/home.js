import React, { Component } from 'react'
import { Grid } from '@material-ui/core';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { Paper } from '@material-ui/core';
import Profile from '../components/Profile';
import Images from '../components/images';
import Search from '../components/search';
import { Image, List } from 'semantic-ui-react'
import {Link} from 'react-router-dom';
import profileImage from '../images/profile.png'
import { Typography } from "@material-ui/core";
//Redux stuff
import { connect } from 'react-redux';


const styles = {
    root: {
        flexGrow: 2,
    },
    paper: {

        textAlign: 'center',
        color: 'white',
    },
};





export class home extends Component {

    state = {
        friends: null
    }

    render() {
        const { classes, user } = this.props;
        let friendsList = user.credentials.friends;
        let recentScreamsMarkup = friendsList ? (
            friendsList.map(friend => { return(
                <List.Item key={friend._id}>
                <Image avatar src={profileImage}/>
                <List.Content>
                    <List.Header> <Link to={`/friend/${friend._id}`} className="btn btn-primary">{friend.firstName + " " + friend.lastName}</Link> </List.Header>
                </List.Content>
            </List.Item>)
            })
        ) : <p>Loading...</p>
        return (
            <div >
                <Grid container spacing={10} direction="row"
                    justify="flex-end"
                    >


                    <Grid item xs={3}  >

                        <Profile />
                        <br />
                        <Search />
                    </Grid>

                    <Grid item xs={6}>
                        <Paper className={classes.paper}><Images /></Paper>
                    </Grid>
                    <Grid item xs={3} sm={3}>
                    <div className="title">        <Typography variant="h6" noWrap>
            My friends
          </Typography> </div>
                        <Paper className={classes.paper2}>
                        <List selection verticalAlign='middle'>
                            {recentScreamsMarkup}
                            </List>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        )
    }
}


home.propTypes = {
    classes: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
}


const mapStateToProps = (state) => ({
    user: state.user,

});

export default connect(mapStateToProps)(withStyles(styles)(home));

