import React, { Component } from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import PropTypes from 'prop-types';
import AppIcon from '../images/alien.png';

import { Link } from 'react-router-dom';

//MUI Stuff
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Textfield from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

//Redux stuff
import { connect } from 'react-redux';
import { signupUser } from '../redux/actions/userActions';

const styles = (theme) => ({
    ...theme.spreadIt
});


class Signup extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            confirmPassword: '',
            firstName: '',
            lastName: '',
            errors: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.UI.errors) {
            this.setState({ errors: nextProps.UI.errors });
        }
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.setState({
            loading: true
        });
        const newUserData = {
            email: this.state.email,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword,
            handle: this.state.handle,
            firstName: this.state.firstName,
            lastName: this.state.lastName

        };
        this.props.signupUser(newUserData, this.props.history);

    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    render() {
        const { classes, UI: { loading } } = this.props;
        const { errors } = this.state;
        return (
            <Grid container className={classes.form}>
                <Grid item sm />
                <Grid item sm>
                    <img src={AppIcon} alt="mask" className={classes.image} />
                    <Typography variant="h4" className={classes.pageTitle}>
                        Sign Up
                </Typography>
                    <form noValidate onSubmit={this.handleSubmit}>
                        <Textfield id="firstName" name="firstName" type="text" label="FirstName" className={classes.Textfield}
                            helperText={errors.firstName}
                            error={errors.firstName ? true : false}
                            value={this.state.firstName} onChange={this.handleChange} fullWidth />
                        <Textfield id="lastName" name="lastName" type="text" label="LastName" className={classes.Textfield}
                            helperText={errors.lastName}
                            error={errors.lastName ? true : false}
                            value={this.state.lastName} onChange={this.handleChange} fullWidth />                        
                        <Textfield id="email" name="email" type="email" label="Email" className={classes.Textfield}
                            helperText={errors.email}
                            error={errors.email ? true : false}
                            value={this.state.email} onChange={this.handleChange} fullWidth />
                        <Textfield id="password" name="password" type="password" label="Password" className={classes.Textfield}
                            helperText={errors.password}
                            error={errors.password ? true : false}
                            value={this.state.password} onChange={this.handleChange} fullWidth />
                        <Textfield id="confirmPassword" name="confirmPassword" type="password" label="Confirm Password" className={classes.Textfield}
                            helperText={errors.confirmPassword}
                            error={errors.confirmPassword ? true : false}
                            value={this.state.confirmPassword} onChange={this.handleChange} fullWidth />
                        {errors.general && (
                            <Typography variant="body2" className={classes.customError}>
                                {errors.general}
                            </Typography>
                        )}
                        <Button type="submit" variant="contained" color="primary" className={classes.button}
                            disabled={loading}>
                            Sign Up
                            {loading &&
                                <CircularProgress size={150} className={classes.progress} />}
                        </Button>
                        <br />
                        <small> Already have an account ? login <Link to="/login"> here </Link></small>
                    </form>
                </Grid>
                <Grid item sm />
            </Grid>
        )
    }
}

Signup.propTypes = {
    classes: PropTypes.object.isRequired,
    signupUser: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    UI: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    user: state.user,
    UI: state.UI
});

const mapActionsToProps = {
    signupUser
}
export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles)(Signup));
