const expect = require('chai').expect;
const request = require('supertest');
const app = require('../app.js');
const mongoose = require("mongoose");

let token1 = null;
let token2 = null;
let id1 = null;
let id2 = null;
describe('USER TEST', () => {


    // create user
    //verified test
    it('OK, creating new user',(done)=> {
        request(app).post('/users')
        .send({firstName:'abdenour0', lastName:'souidi0',
        email:'abdenour0@gmail.com',password:'abdenour0',confirmPassword:'abdenour0'})
        .then((res)=>{
            const body = res.body;

            expect(body).to.contain.property('message_signup');
            expect(body).to.contain.property('token');
            expect(body).to.contain.property('id');
            id1 = body.id;
            done();
        }).catch((err)=> done(err))
    })


    //login
    //verified test
    it('OK, login into an account',(done)=> {
        request(app).post('/users/login')
        .send({email : "abdenour0@gmail.com", password : "abdenour0"})
        .then((res)=>{
            const body = res.body;

            expect(body).to.contain.property('message_login');
            expect(body).to.contain.property('token');
            
            token1 = body.token;
            done();
        }).catch((err)=> done(err))
    })


    //get all users
    //verified test
    it('OK, get all users',(done)=> {
        request(app).get('/users/all')
        .set('Authorization',"Bearer "+token1)
        .then((res)=>{
            const body = res.body;

            expect(body).to.contain.property('count');
            expect(body).to.contain.property('users');
            done();
        }).catch((err)=> done(err))
    })

    
    it('OK, get connected user',(done)=> {
        request(app).get('/users/user')
        .set('Authorization',"Bearer "+token1)
        .then((res)=>{
            const body = res.body;
            expect(body).to.contain.property('user');
            expect(body).to.contain.property('request');
          
            done();
        }).catch((err)=> done(err))
    })

    //get 1 user
    //verified test 
    //id and token(id in the token) must be equal
    it('OK, Get 1 user',(done)=> {
        request(app).get('/users/one/'+id1)
        .set('Authorization',"Bearer "+token1)
        .then((res)=>{
            const body = res.body;

            expect(body).to.contain.property('user');
            expect(body).to.contain.property('request');
            done();
        }).catch((err)=> done(err))
    })


     //patch
     //verified test 
     it('OK, Modify 1 or more properties',(done)=> {
        request(app).patch('/users/')
        .set('Authorization',"Bearer "+token1)
        .send({firstName:"Mr. abdenour0"})
        .then((res)=>{
            const body = res.body;

            expect(body).to.contain.property('message_patch');
           
            done();
        }).catch((err)=> done(err))
    })


    //put
    //verified test 
    it('OK, Modify all properties',(done)=> {
        request(app).put('/users/')
        .set('Authorization',"Bearer "+token1)
        .send({ firstName:"Mr. Abdenour00",
    lastName:"Mr. SOUIDI00",email:"abdenour00@gmail.com",password:"abdenour00"})
        .then((res)=>{
            const body = res.body;

            expect(body).to.contain.property('message_put');
           
            done();
        }).catch((err)=> done(err))
    })


    // create user 2
    //verified test
    it('OK, creating new user 2',(done)=> {
        request(app).post('/users')
        .send({firstName:'ghassen0', lastName:'ghassen0',
        email:'ghassen0@gmail.com',password:'ghassen0',confirmPassword:'ghassen0'})
        .then((res)=>{
            const body = res.body;

            expect(body).to.contain.property('message_signup');
            expect(body).to.contain.property('token');
            expect(body).to.contain.property('id');
            id2 = body.id;
            token2 = body.token;
            done();
        }).catch((err)=> done(err))
    })

    //add invitation
//verified test
it('OK, Add invitation',(done)=> {
    request(app).post('/invitations')
    .set('Authorization',"Bearer "+token2)
    .send({idReceiver:id1})
    .then((res)=>{
        const body = res.body;

        expect(body).to.contain.property('message_addFriend');
        
        done();
    }).catch((err)=> done(err))
})

    //accept a friend
    it('OK, accept friend',(done)=> {
        request(app).put('/users/accept')
        .set('Authorization',"Bearer "+token1)
        .send({senderId:id2})
        .then((res)=>{
            const body = res.body;

            expect(body).to.contain.property('message_accept');
           
            done();
        }).catch((err)=> done(err))
    })

    //get friends
    //verified test
    it('OK, Get all friends of users',(done)=> {
        request(app).get('/users/friends')
        .set('Authorization',"Bearer "+token1)
        .then((res)=>{
            const body = res.body;

            expect(body).to.contain.property('friends');
            expect(body).to.contain.property('request');
           
            done();
        }).catch((err)=> done(err))
    })

    //delete user
    //verified test
    it('OK, delete user',(done)=> {
        request(app).delete('/users/')
        .set('Authorization',"Bearer "+token1)
        .then((res)=>{
            const body = res.body;

            expect(body).to.contain.property('message_delete');
           
            done();
        }).catch((err)=> done(err))
    })

    //delete user 2
    //verified test
    it('OK, delete user 2',(done)=> {
        request(app).delete('/users/')
        .set('Authorization',"Bearer "+token2)
        .then((res)=>{
            const body = res.body;

            expect(body).to.contain.property('message_delete');
           
            done();
        }).catch((err)=> done(err))
    })


    

})