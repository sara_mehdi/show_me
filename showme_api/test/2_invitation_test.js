const expect = require('chai').expect;
const request = require('supertest');
const app = require('../app.js');
const mongoose = require("mongoose");
let token1 = null;
let id1 = null;
describe('INVITATION TEST', () => {

    // create user for invitation
    //verified test
    it('OK, creating new user',(done)=> {
        request(app).post('/users')
        .send({firstName:'abdenour0', lastName:'souidi0',
        email:'abdenour0@gmail.com',password:'abdenour0',confirmPassword:'abdenour0'})
        .then((res)=>{
            const body = res.body;

            expect(body).to.contain.property('message_signup');
            expect(body).to.contain.property('token');
            expect(body).to.contain.property('id');
            id1 = body.id;
            token1 = body.token;
            done();
        }).catch((err)=> done(err))
    })



//get all invitation
//verified test
it('OK, Get all invitations',(done)=> {
    request(app).get('/invitations')
    .set('Authorization',"Bearer "+token1)
    .then((res)=>{
        const body = res.body;

        expect(body).to.contain.property('count');
        expect(body).to.contain.property('invitation');
        done();
    }).catch((err)=> done(err))
})


//get all invitations of a user
//verified test
it('OK, Get all invitations of a user',(done)=> {
    request(app).get('/invitations/inviUser')
    .set('Authorization',"Bearer "+token1)
    .then((res)=>{
        const body = res.body;
        expect(body).to.length(0)
        done();
    }).catch((err)=> done(err))
})



//get all invitations sent by a user
//verified test
it('OK, Get all invitations sent by a user',(done)=> {
    request(app).get('/invitations/send')
    .set('Authorization',"Bearer "+token1)
    .then((res)=>{
        const body = res.body;

        expect(body).to.contain.property('count');
        expect(body).to.contain.property('invitation');
        done();
    }).catch((err)=> done(err))
})




})
