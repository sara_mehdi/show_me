const mongoose = require('mongoose'), Schema = mongoose.Schema;

const usersSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    firstName: {type : String, require : true} ,
    lastName: {type : String, require : true} ,
    profilePhoto :{type: Schema.Types.ObjectId, ref: 'Picture'} ,
    email: {type : String, require : true, unique : true ,
    match :/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/  } ,
    password: {type : String, require : true} ,
    friends :[{type: Schema.Types.ObjectId, ref: 'User' }]
});

module.exports = mongoose.model('User', usersSchema);  