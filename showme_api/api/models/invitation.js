const mongoose = require('mongoose'), Schema = mongoose.Schema;

const invitationSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    status: {type : Boolean, require : true} ,
    idSender: {type: Schema.Types.ObjectId, ref: 'User', require : true} ,
    idReceiver: {type: Schema.Types.ObjectId, ref: 'User', require:true}
});

module.exports = mongoose.model('Invitation', invitationSchema);  