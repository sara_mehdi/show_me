const mongoose = require('mongoose'),Schema = mongoose.Schema;

const pictureSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    legend: {type : String, require : true} ,
    time: {type : String, require : true} ,
    visibility: {type : String , require:true},
    userId: {type: Schema.Types.ObjectId, ref: 'User', require : true},
    url:{type: String, require: true} ,
    picture : {type : Buffer}
});

module.exports = mongoose.model('Picture', pictureSchema);  