const Picture = require('../models/picture.js');
const mongoose = require('mongoose');
const fs = require('fs');
const User = require("../models/user.js");

exports.add_picture = (req, res, next) => {
    const encoding = fs.readFileSync(req.file.path).toString('base64')
    const picture = new Picture({
        _id: new mongoose.Types.ObjectId(),
        legend: req.body.legend,
        time: new Date(),
        visibility: req.body.visibility,
        userId: req._id,
        url: req.file.path,
        picture: Buffer.from(encoding, 'base64') //
    })

    picture
        .save()
        .then(result => {

            res.status(201).json({
                message: 'created picture successfully',
                create_pictue: {
                    id: result._id,
                    legend: result.legend,
                    time: result.time,
                    visibility: result.visibility,
                    userId: result.userId,
                    url: result.url,
                    picture: new Buffer(result.picture, 'binary').toString('base64')
                }
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        });
    res.sen
}
///////////////

exports.get_pictures_forUser = (req, res, next) => {
    const userId = req.params.id
    let query = { userId: userId }
    let isFriend=-2;
    var callMyPromise = async () => {
        let user = await User.findById(req._id)
        return user;
    }

    callMyPromise().then(function (result) {
         isFriend = result.friends.indexOf(userId);
         if(userId.toString()===req._id.toString()){
            query = { userId: userId }
         }
         else if (isFriend > -1) {
            query = { userId: userId,$or: [ {visibility: 'amis'}, { visibility: 'public'} ] }
        } else {
            query = { userId: userId, visibility: 'public' }
        }
        Picture
            .find(query)
            .select('_id legend time visibility userId url picture')
            .exec()
            .then(results => {
                const result = {
                    count: results.length,
                    pictures: results.map(p => {
                        return {
                            _id: p._id,
                            legend: p.legend,
                            time: p.time,
                            visibility: p.visibility,
                            userId: p.userId,
                            url: p.url,
                            base64: new Buffer(p.picture, 'binary').toString('base64')
    
                        }
                    })
                };
    
                res.status(200).json(result);
            })
            .catch(err => {
                res.status(500).json({
                    message: err
                })
            });

    });

}

/////////////
exports.get_picture_byId = (req, res, next) => {
    const userId = req._id
    const pictureId = req.params.id
    Picture
        .find({ _id: pictureId })
        .select('_id legend time visibility userId url picture')
        .exec()
        .then(results => {
            const result = {

                pictures: results.map(p => {
                    return {
                        _id: p._id,
                        legend: p.legend,
                        time: p.time,
                        visibility: p.visibility,
                        userId: p.userId,
                        url: p.url,
                        base64: new Buffer(p.picture, 'binary').toString('base64')

                    }
                })
            };

            res.status(200).json(result);
        })
        .catch(err => {
            res.status(500).json({
                message: err
            })
        });
}

//////////
exports.get_pictures_forALLuser = (req, res, next) => {
    Picture
        .find({ visibility: 'public' })  //la version d'avant elle n'a pas where
        .select('_id legend time visibility userId url picture')
        .exec()
        .then(results => {

            var promise = (userId, status) => {
                return new Promise((resolve, reject) => {
                    User.findOne({ _id: userId },
                        (err, user) => {
                            let newUser = {
                                _id: user._id,
                                firstName: user.firstName,
                                lastName: user.lastName,
                                email: user.email
                            }
                            err ? reject(err) : resolve(newUser);
                        }
                    );
                });
            };

            var callMyPromise = async () => {
                let newArrays = [];
                for (var i = 0; i < results.length; i++) {
                    var result = await promise(results[i].userId);
                    results[i].userId = result;
                    newArrays.push({
                        _id: results[i]._id,
                        legend: results[i].legend,
                        time: results[i].time,
                        visibility: results[i].visibility,
                        user: result,
                        url: results[i].url,
                        picture: results[i].picture
                    })
                }
                return newArrays;
            }

            //call the function callMyPromise()
            callMyPromise().then(function (result) {


                const newResult = {
                    count: result.length,
                    pictures: result.map(p => {
                        return {
                            _id: p._id,
                            legend: p.legend,
                            time: p.time,
                            visibility: p.visibility,
                            user: p.user,
                            url: p.url,
                            base64: new Buffer(p.picture, 'binary').toString('base64')

                        }
                    })
                };

                res.status(200).json(newResult);
            });

        })
        .catch(err => {
            res.status(500).json({
                message: err
            })
        });
}

//////////////////

exports.picture_modify_all_properties = (req, res, next) => {
    const userId = req._id
    const pictureId = req.params.pictureId
    Picture.findByIdAndUpdate({ _id: pictureId, userId: userId }, req.body)
        .exec()
        .then(result => {
            res.status(200).json({
                message: "picture updated",

            });
        })
        .catch(err => {
            res.status(500).json({
                message: err
            })
        });
}

////////////////////
exports.detete_allPictures_forUser = (req, res, next) => {
    const userId = req._id;
    Picture.remove({ userId: userId })
        .exec()
        .then(result => {
            res.status(200).json({
                message: "pictureS deleted",

            });
        })
        .catch(err => {
            res.status(500).json({
                message: err
            })
        });
}

////////////////////
exports.detete_onePicture_forUser = (req, res, next) => {
    const userId = req._id;
    const pictureId = req.params.pictureId;
    Picture.remove({ userId: userId, _id: pictureId })
        .exec()
        .then(result => {
            res.status(200).json({
                message: "picture deleted",

            });
        })
        .catch(err => {
            res.status(500).json({
                message: err
            })
        });
}