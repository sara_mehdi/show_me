const Invitation = require("../models/invitation.js");
const mongoose = require("mongoose");
const User = require("../models/user.js");
exports.add_invitation = (req, res, next) => {
  const invitation = new Invitation({
    _id: new mongoose.Types.ObjectId(),
    status: false,
    idSender: req._id,
    idReceiver: req.body.idReceiver
  });

  invitation
    .save()
    .then((result) => {
      res.status(201).json({
        message_addFriend: "Invitation created successfully"
      });
    })
    .catch((error) => {
      res.status(500).json({
        error
      });
    });
};

///////////////

exports.get_invitations = (req, res, next) => {
  Invitation.find()
    .select("_id status idSender idReceiver")
    .exec()
    .then((results) => {
      const result = {
        count: results.length,
        invitation: results.map((p) => {
          return {
            _id: p._id,
            idSender: p.idSender,
            idReceiver: p.idReceiver,
            status: p.status
          };
        })
      };

      res.status(200).json(result);
    })
    .catch((err) => {
      res.status(500).json({
        message: err
      });
    });
};

////////////////
exports.get_invitations_forUser = (req, res, next) => {
  Invitation.find({ idReceiver: req._id, status: false })
    .select("_id status idSender idReceiver")
    .exec()
    .then((results) => {
      var promise = (userId,status) => {
        return new Promise((resolve, reject) => {
          User.findOne({ _id: userId },
            (err, user) => {
              let sender = {
                _id: user._id,
                firstName: user.firstName,
                lastName: user.lastName,
                status:status
                           }
              err ? reject(err) : resolve(sender);
            }
          );
        });
      };

      var callMyPromise = async () => {
        for (var i = 0; i < results.length; i++) {
          var result = await promise(results[i].idSender,results[i].status);
          results[i] = result;
        }
        return results;
      }

      //call the function callMyPromise()
      callMyPromise().then(function (result) {
        return res.status(200).json(results);
      });

     
    })
    .catch((err) => {
      res.status(500).json({
        message: err
      });
    });
};

////////////////
exports.get_invitations_SendforUser = (req, res, next) => {
  Invitation.find({ idSender: req._id, status: false })
    .select("_id status idSender idReceiver")
    .exec()
    .then((results) => {
      const result = {
        count: results.length,
        invitation: results.map((p) => {
          return {
            _id: p._id,
            idSender: p.idSender,
            idReceiver: p.idReceiver,
            status: p.status
          };
        })
      };

      res.status(200).json(result);
    })
    .catch((err) => {
      res.status(500).json({
        message: err
      });
    });
};

/////////////////
exports.delete_invitation_byId = (req, res, next) => {
  const _id = req.body.idInvitation;
  Invitation.remove({ _id: _id })
    .exec()
    .then((result) => {
      res.status(200).json({
        message_deleteInvitation: "Invitation has been deleted"
      });
    })
    .catch((err) => {
      res.status(500).json({
        message: err
      });
    });
};

