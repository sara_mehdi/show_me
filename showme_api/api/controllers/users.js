const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const Invitation = require("../models/invitation.js");
const Picture = require('../models/picture.js');
const { validateSignupData, validateLoginData } = require('../validator');
const fs = require('fs');

exports.users_get = (req, res, next) => {
    User.find()
        .select("_id firstName lastName email friends profilePhoto")
        .exec()
        .then((results) => {
            const result = {
                count: results.length,
                users: results.map((u) => {
                    return {
                        _id: u._id,
                        first_name: u.firstName,
                        last_name: u.lastName,
                        email: u.email,
                        friends: u.friends,
                        request: {
                            type: "GET",
                            url: "localhost:8081/users/all"
                        }
                    };
                })
            };
            return res.status(200).json(result);
        })
        .catch((err) => {
            return res.status(500).json({
                message: err
            });
        });
};

exports.users_friends = (req, res, next) => {
    const userId = req._id;
    return User.findById(userId)
        .select("friends")
        .exec()
        .then((friends) => {
            if (friends) {
                return res.status(200).json({
                    friends,
                    request: {
                        type: "GET",
                        description: "Get users's contacts",
                        url: "localhost:8081/users/friends"
                    }
                });
            } else {
                return res.status(404).json({
                    message: "Invalide ID"
                });
            }
        })
        .catch((err) => {
            return res.status(500).json({
                error: err
            });
        });
};

//////////////
exports.users_signup = (req, res, next) => {
    const { valid, errors } = validateSignupData(req.body);
    if (!valid) return res.status(400).json(errors);
    User.find({ email: req.body.email })
        .exec()
        .then((user) => {
            if (user.length >= 1) {
                return res.status(409).json({
                    email: "Email already used"
                });
            } else {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).json({
                            error: "An error occured when tempting to hash the password" + err
                        });
                    } else {
                        const user = new User({
                            _id: new mongoose.Types.ObjectId(),
                            firstName: req.body.firstName,
                            lastName: req.body.lastName,
                            email: req.body.email,
                            profilePhoto: null,
                            password: hash
                        });
                        user
                            .save()
                            .then((result) => {
                                console.log("[INFO]: New user joined");
                                const token = jwt.sign(
                                    {
                                        email: user.email,
                                        userId: user._id
                                    },
                                    "UPEC-REST",
                                    {
                                        expiresIn: "1h"
                                    }
                                );
                                return res.status(201).json({
                                    id : user._id,
                                    message_signup: "User created successfully",
                                    token: token
                                });
                            })
                            .catch((err) => {
                                return res.status(500).json({
                                    general: 'Something went wrong, please try again'
                                });
                            });
                    }
                });
            }
        });
};

//////////

exports.users_login = (req, res, next) => {

    const user = {
        email: req.body.email,
        password: req.body.password
    };

    const { valid, errors } = validateLoginData(user);
    if (!valid) return res.status(400).json(errors);
    User.find({ email: req.body.email })
        .exec()
        .then((users) => {
            if (users.length < 1) {
                return res.status(404).json({
                    email: "Authentication failed email doesn't match to any user"
                });
            }
            //continuer
            bcrypt.compare(req.body.password, users[0].password).then((result) => {
                if (result) {
                    //token
                    const token = jwt.sign(
                        {
                            email: users[0].email,
                            userId: users[0]._id
                        },
                        "UPEC-REST",
                        {
                            expiresIn: "1h"
                        }
                    );
                    console.log(`[INFO]: ${users[0]._id}  user contected`);
                    return res.status(200).json({
                        message_login: "Authenticated Successfully",
                        token: token
                    });
                } else {
                    return res.status(404).json({
                        password: "Authentication failed password doesn't match"
                    });
                }
                res.status(401).json({
                    general: 'Authentication failed! please try again '
                });
            });
        })
        .catch((err) => {
            return res.status(500).json({
                general: 'Something went wrong, please try again'
            });
        });
};

/////////////////

exports.users_get_byId = (req, res, next) => {
    const { id } = req.params;
    User.findById(id)
        .select("firstName lastName email password _id friends profilePhoto ")
        .exec()
        .then(async (doc) => {
            let userWithPhoto = doc
            if (doc) {
                const p = await Picture.findById(doc.profilePhoto)
                if (p) {
                    userWithPhoto = {
                        firstName: doc.firstName,
                        lastName: doc.lastName,
                        email: doc.email,
                        _id: doc._id,
                        friends: doc.friends,
                        profilePhoto: new Buffer(p.picture, 'binary').toString('base64')
                    }
                }
                return res.status(200).json({
                    user: userWithPhoto,
                    request: {
                        type: "GET",
                        description: "Get user by id ",
                        url: "localhost:8081/users/one/:id"
                    }
                });
            } else {
                return res.status(404).json({
                    message: "Invalide ID"
                });
            }
        })
        .catch((err) => {
            return res.status(500).json({
                error: err
            });
        });
};


exports.users_modify = (req, res, next) => {

    User.findByIdAndUpdate({ _id: req._id }, req.body)
        .exec()
        .then((result) => {
            res.status(200).json({
                message_patch: "user updated"
            });
        })
        .catch((err) => {

            return res.status(500).json({
                message: err
            });
        });
};
////////////////
exports.users_modify_allProperties = (req, res, next) => {
    if (
        req.body.firstName &&
        req.body.lastName &&
        req.body.email &&
        req.body.password
    ) {
        const id = req._id;
        User.findByIdAndUpdate({ _id: id }, req.body)
            .exec()
            .then((u) => {
                if (u !== null) {
                    if (u._id.toString() !== req._id.toString()) {
                        return res.status(409).json({
                            param: "email",
                            error: "Email already used"
                        });
                    }
                }

                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).json({
                            error: "An error occured when tempting to hash the password" + err
                        });
                    } else {
                        const id = req._id;
                        req.body.password = hash;
                        User.findByIdAndUpdate({ _id: id }, req.body)
                            .exec()
                            .then((result) => {
                                return res.status(200).json({
                                    message_put: "user updated"
                                });
                            })
                            .catch((err) => {
                                return res.status(500).json({
                                    message: err
                                });
                            });
                    }
                });


            });
        ////


    } else {
        return res.status(500).json({
            message: "You should send all inputs"
        });
    }
};
////////////////

exports.users_delete = (req, res, next) => {
    const _id = req._id;
    User.remove({ _id: _id })
        .exec()
        .then(async () => {
            //delete pictures
            Picture.remove({ userId: _id }).exec();
            //delete this user from other users
            const f = await User.find();

            for (let i = 0; i < f.length; i++) {
                const tabuser = f[i].friends;

                let index = tabuser.indexOf(_id.toString())
                tabuser.splice(index, 1);

                f[i].save();

            }

            //delete invitation where idsender or idreceiver = _id

            const inv = await Invitation.find();
            for (let i = 0; i < inv.length; i++) {
                if (inv[i].idSender.toString() === _id.toString() || inv[i].idReceiver.toString() === _id.toString()) {
                    Invitation.remove({ _id: inv[i]._id }).exec()

                }

            }
            console.log(`[INFO]: User ${_id} has been deleted`);
            return res.status(200).json({
                message_delete: "User has been deleted"
            });
        })
        .catch((err) => {
            console.log(err);
            return res.status(500).json({
                message: err
            });
        });
};

///////////////
exports.accept = async (req, res, next) => {
    const senderId = req.body.senderId;
    const userId = req._id;
    Invitation.findOneAndUpdate(
        { idSender: senderId, idReceiver: userId },
        { status: true }
    )
        .exec()
        .then(async () => {
            let user1 = await User.findById(userId);
            const tab = user1.friends;
  
            if (tab.indexOf(senderId) != -1) {
            } else {
                tab.push(senderId);
                user1.update({ friends: tab });
                user1.save();
            }
  
  
            //add this to the other
            let user2 = await User.findById(senderId)
  
  
            const tab2 = user2.friends;
            if (tab2.indexOf(userId) != -1) {
            } else {
                tab2.push(userId);
                user2.update({ friends: tab2 });
                user2.save();
  
            }
  
            return res.status(200).json({
                message_accept: "Contact added to your list"
            });
  
        })
        .catch((err) => {
            return res.status(500).json({
                message: "error ."
            });
        });
  };
  
  //////////////////////
  

//////////////////
exports.users_delete_friend = (req, res, next) => {
    const _id = req._id;
    const freindId = req.params.id;

    User.findById({ _id: _id })
        .exec()
        .then(async (u) => {

            //delete the friend from the current user
            const friendsOfUser = await u.friends
            let index = await friendsOfUser.indexOf(freindId.toString())
            await friendsOfUser.splice(index, 1);
            await u.save();

            //delete this user from the friend
            const fr = await User.findById({ _id: freindId })
            const friendsOfUser2 = await fr.friends
            let index2 = await friendsOfUser2.indexOf(_id.toString())
            await friendsOfUser2.splice(index2, 1);
            await fr.save();


            //delete the invitation
            const inv = await Invitation.find();
            for (let i = 0; i < inv.length; i++) {
                if ((inv[i].idSender.toString() === _id.toString() || inv[i].idReceiver.toString() === _id.toString())
                    && (inv[i].idSender.toString() === freindId.toString() || inv[i].idReceiver.toString() === freindId.toString())) {

                    Invitation.deleteOne({ _id: inv[i]._id }).exec()

                }
            }


            return res.status(200).json({
                message_delete_friend: "Friend deleted"
            });

        })
        .catch((err) => {
            return res.status(500).json({
                message: err
            });
        });


};
// added by ghassen
exports.getAuthenticatedUser = (req, res, next) => {
    const { id } = req._id;
    User.findById(req._id)
        .select("firstName lastName email  _id friends profilePhoto")
        .exec()
        .then((doc) => {
            if (doc) {
                var promiseToGetFriend = (userId) => {
                    return new Promise((resolve, reject) => {
                        User.findOne({ _id: userId },
                            (err, user) => {
                                const friend = {
                                    firstName: user.firstName,
                                    lastName: user.lastName,
                                    email: user.email,
                                    _id: user._id,
                                    friends: user.friends,
                                    profilePhoto: user.profilePhoto
                                }
                                err ? reject(err) : resolve(friend);
                            }
                        );
                    });
                };

                var callMyPromiseToGetFriend = async () => {
                    for (var i = 0; i < doc.friends.length; i++) {
                        let result = await promiseToGetFriend(doc.friends[i]._id);
                        doc.friends[i] = result;
                    }
                    return doc;
                }

                callMyPromiseToGetFriend().then(function (result) {

                    var promiseToGetphoto = (photoId) => {
                        return new Promise((resolve, reject) => {
                            Picture.findOne({ _id: photoId },
                                (err, p) => {
                                    let profile_photo=null;
                                    if(p!=null){
                                     profile_photo = new Buffer(p.picture, 'binary').toString('base64')
                                    }
                                    err ? reject(err) : resolve(profile_photo);
                                }
                            );
                        });
                    };


                    var callMyPromiseToGetPhoto = async () => {
                        if (result.profilePhoto) {
                            let result2 = await promiseToGetphoto(result.profilePhoto);
                            doc.profilePhoto = [];

                            const userWithPhoto = {
                                firstName: doc.firstName,
                                lastName: doc.lastName,
                                email: doc.email,
                                _id: doc._id,
                                friends: doc.friends,
                                profilePhoto: result2
                            }
                            return userWithPhoto;
                        }
                        else return doc


                    }


                    callMyPromiseToGetPhoto().then(function (result) {

                        return res.status(200).json({
                            user: result,
                            request: {
                                type: "GET",
                                description: "Get user by id ",
                                url: "localhost:8081/users/one/:id"
                            }
                        });
                    });

                });


            } else {
                return res.status(404).json({
                    message: "Invalide ID"
                });
            }
        }
        )
        .catch((err) => {
            return res.status(500).json({
                error: err
            });
        });
};

exports.add_profile_picture = (req, res, next) => {
    const encoding = fs.readFileSync(req.file.path).toString('base64');
    const picture = new Picture({
        _id: new mongoose.Types.ObjectId(),
        legend: "profile photo",
        time: new Date(),
        visibility: "public",
        userId: req._id,
        url: req.file.path,
        picture: Buffer.from(encoding, 'base64') //
    })
    picture
        .save()
        .then(result => {
            var query = { profilePhoto: result._id };
            User.findOneAndUpdate({ _id: req._id }, query, (err, doc) => {

            });

            res.status(201).json({
                message: 'created picture successfully',
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        });
    res.sen
}




