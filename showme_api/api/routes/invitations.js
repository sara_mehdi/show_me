const express = require("express");
const router = express.Router();
const InvitationsController = require("../controllers/invitations");
const checkAuth = require("../checkToken/check-auth");

//add invitation
router.post("/", checkAuth, InvitationsController.add_invitation);

//get ALL invitation
router.get("/", checkAuth, InvitationsController.get_invitations);


router.delete("/",checkAuth, InvitationsController.delete_invitation_byId);

//get ALL invitation for user
router.get("/inviUser",checkAuth, InvitationsController.get_invitations_forUser
);

//get ALL invitation sent by user
router.get("/send/", checkAuth,InvitationsController.get_invitations_SendforUser
);





module.exports = router;
