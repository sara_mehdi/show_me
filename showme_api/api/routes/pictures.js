const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Picture = require("../models/picture.js");
const PicturesController = require("../controllers/pictures");
const multer = require("multer");
const checkAuth = require("../checkToken/check-auth");

const storage = multer.diskStorage({
  destination: function(res, file, cb) {
    cb(null, "./uploads/");
  },

  filename: function(req, file, cb) {
    cb(null, Date.now() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 6 //6mb max size
  },
  fileFilter: fileFilter
});

//add picture
router.post(
  "/",
  upload.single("picture"),
  checkAuth,
  PicturesController.add_picture
);

//get all pictures of a user
router.get("/all/:id", checkAuth, PicturesController.get_pictures_forUser);

//get a picture
router.get("/one/:id", checkAuth, PicturesController.get_picture_byId);

//get a ALL pictures off ALL users
router.get("/", checkAuth, PicturesController.get_pictures_forALLuser);

//modify picture
router.put("/:pictureId", checkAuth, PicturesController.picture_modify_all_properties);

//delete all pictureS of user
router.delete("/all", checkAuth, PicturesController.detete_allPictures_forUser);

//delete one picture of user
router.delete("/one/:pictureId", checkAuth, PicturesController.detete_onePicture_forUser);

module.exports = router;
