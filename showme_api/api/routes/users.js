const express = require("express");
const router = express.Router();
const UsersController = require("../controllers/users");
const checkAuth = require("../checkToken/check-auth");
const multer = require("multer");
const storage = multer.diskStorage({
    destination: function(res, file, cb) {
      cb(null, "./uploads/");
    },
  
    filename: function(req, file, cb) {
      cb(null, Date.now() + file.originalname);
    }
  });
  
  const fileFilter = (req, file, cb) => {
    if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };
  const upload = multer({
    storage: storage,
    limits: {
      fileSize: 1024 * 1024 * 6 //6mb max size
    },
    fileFilter: fileFilter
  });

//avoir tt les utilisateurs
router.get("/all", checkAuth, UsersController.users_get);

//creer un compte
router.post("/", UsersController.users_signup);

//se connecter
router.post("/login", UsersController.users_login);

//avoir un utilisateur a partir de son identifiant
router.get("/one/:id", checkAuth, UsersController.users_get_byId);

//modifier un utilisateur un ou plusieurs champs
router.patch("/", checkAuth, UsersController.users_modify);

//modifier un utilisateur , tous les champs
router.put("/", checkAuth, UsersController.users_modify_allProperties);

//accepter un ami
router.put("/accept", checkAuth, UsersController.accept);

//supprimer un utilisateur
router.delete("/", checkAuth, UsersController.users_delete);

//supprimer un ami
router.delete("/deletefriend/:id", checkAuth, UsersController.users_delete_friend);

// get user's friends
router.get("/friends", checkAuth, UsersController.users_friends);

//avoir un utilisateur a partir de son identifiant
router.get("/user", checkAuth, UsersController.getAuthenticatedUser);

//
router.post("/post_photo_profile",upload.single("picture"), checkAuth, UsersController.add_profile_picture);
module.exports = router;

