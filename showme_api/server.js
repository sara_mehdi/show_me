const http = require("http");
const app = require("./app.js");

const port = process.env.PORT || 8081;

const server = http.createServer(app);

server.listen(port);
console.log(`[INFO]: Server started on port 8081`);
