const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyPaser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const usersRoutes = require("./api/routes/users.js");
const picturesRoutes = require("./api/routes/pictures.js");
const invitationsRoutes = require("./api/routes/invitations.js");


const swaggerDocument = require('./swagger.json');

const swaggerUi = require('swagger-ui-express');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// mongoose.connect("mongodb://localhost/test", {
//   useCreateIndex: true,
//   useNewUrlParser: true,
//   useUnifiedTopology: true
// });

mongoose.connect("mongodb://localhost/test3", {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true
});
var db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function callback() {
  console.log(`[INFO]: Connected to database`);
});

exports.test = function(req, res) {
  res.render("test");
};

mongoose.Promise = global.Promise;
app.use(morgan(`[INFO]: :date[clf] :method :url :status :res[content-length] - :response-time ms`))

//parser les entrants json
app.use(bodyPaser.urlencoded({ extended: false }));
app.use(bodyPaser.json());

app.use(cors());

//routes
app.use("/users", usersRoutes);
app.use("/pictures", picturesRoutes);
app.use("/invitations", invitationsRoutes);

//error handling
app.use((req, res, next) => {
  const error = new Error("Not Found");
  error.status = 404;
  next(error);
});

//DB error
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;
